FROM ruby:2.6.5


RUN apt-get update
RUN apt-get install -y nodejs

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY application/blog/ ./
RUN bundle install

CMD ["rails", "s", "-b", "0.0.0.0", "-p", "3000"]